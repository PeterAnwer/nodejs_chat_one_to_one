const mysql = require('mysql');
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {cors: {origin: "*"}});
var users = [];

server.listen(8080, function () {
    console.log('Port: 8080');
});

function setStatus(s) {
    io.emit('status', s);
}

const
    con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "chat_db"
    });

con.connect(function (err) {
    if (err) throw err;

    io.on('connection', function (socket) {

        socket.on('login', function (name) {
            socket.emit('usersSockets', {'users': users, 'currentSocket': socket.id});
            users[users.length] = {'socketId': socket.id, 'name': name};
        });

        socket.on('getUsers', function () {
            socket.emit('usersSockets', {'users': users, 'currentSocket': socket.id});
        });

        socket.on('input', function (data) {

            let name = data.name;
            let message = data.message;
            let whiteSpacePattern = /^\s*$/;

            if (whiteSpacePattern.test(name) || whiteSpacePattern.test(message)) {
                setStatus('Message and name are required');
            } else {
                io.to(data.to).to(data.currentSocket).emit('output', [data]);
                setStatus({'message': 'Sent!', 'clear': true});
            }

        });
    });
});